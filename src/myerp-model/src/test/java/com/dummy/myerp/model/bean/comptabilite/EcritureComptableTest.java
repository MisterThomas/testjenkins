package com.dummy.myerp.model.bean.comptabilite;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;


@Tag("EcritureComptableTest")
@DisplayName("Réussir et respecter l'ecriture dans la comptabilité.")
@ExtendWith(LoggingExtension.class)
public class EcritureComptableTest {


    private static Instant startedAt;

    private EcritureComptable ecritureComptable;


    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @BeforeEach
    public void initEcritureComptable() {
        logger.info("Appel avant chaque test");
        ecritureComptable = new EcritureComptable();
    }

    @AfterEach
    public void undefEcritureComptable() {
        logger.info("Appel après chaque test");
        ecritureComptable = null;
    }

    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tous les tests");
        startedAt = Instant.now();
    }

    @AfterAll
    public static void showTestDuration() {
        System.out.println("Appel après tous les tests");
        final Instant endedAt = Instant.now();
        final long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des tests : {0} ms", duration));
    }



    @Test
    public void testEcritureComptable() {

        // GIVEN
        final String libelle = "Test libellé 1";
        final int credit = 101;
        final BigDecimal debit = null;
        final  CompteComptable object = null;


        // WHEN
        LigneEcritureComptable ligneEcritureComptable = new LigneEcritureComptable();
        ligneEcritureComptable.setCompteComptable(object);
        ligneEcritureComptable.setLibelle(libelle);
        ligneEcritureComptable.setCredit(BigDecimal.valueOf(credit));
        ligneEcritureComptable.setDebit(debit);

        // THEN
        Assert.assertEquals(ligneEcritureComptable.toString(),
                "LigneEcritureComptable" +
                        "{compteComptable=null, libelle='Test libellé 1', debit=null, credit=101}");
    }


}
