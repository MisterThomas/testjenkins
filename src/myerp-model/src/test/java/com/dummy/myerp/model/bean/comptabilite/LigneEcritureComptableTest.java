package com.dummy.myerp.model.bean.comptabilite;


import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;

@Tag("LigneEcritureComptableTest")
@DisplayName("Réussir a verifier une ligne ecriture comptable.")
@ExtendWith(LoggingExtension.class)
public class LigneEcritureComptableTest {

    private  LigneEcritureComptable ligneEcritureComptable;

    private static Instant startedAt;

    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @BeforeEach
    public void initLigneEcritureComptable () {
        logger.info("Appel avant chaque test");
        ligneEcritureComptable = new LigneEcritureComptable();
    }

    @AfterEach
    public void undefLigneEcritureComptable() {
        logger.info("Appel après chaque test");
        ligneEcritureComptable = null;
    }

    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tous les tests");
        startedAt = Instant.now();
    }

    @AfterAll
    public static void showTestDuration() {
        System.out.println("Appel après tous les tests");
        final Instant endedAt = Instant.now();
        final long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des tests : {0} ms", duration));
    }

    @Test
    public void testLigneEcritureComptable() {

        // GIVEN
        final String libelle = "Test libellé 1";
        final int credit = 101;
        final BigDecimal debit = null;
        final  CompteComptable object = null;


        // WHEN
        LigneEcritureComptable ligneEcritureComptable = new LigneEcritureComptable();
        ligneEcritureComptable.setCompteComptable(object);
        ligneEcritureComptable.setLibelle(libelle);
        ligneEcritureComptable.setCredit(BigDecimal.valueOf(credit));
        ligneEcritureComptable.setDebit(debit);

        // THEN
        Assert.assertEquals(ligneEcritureComptable.toString(),
                "LigneEcritureComptable" +
                        "{compteComptable=null, libelle='Test libellé 1', debit=null, credit=101}");
    }
}
